import {useState, useEffect} from 'react'
// import {Container} from 'react-bootstrap'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import ProductView from './components/ProductView'
import Home from './pages/Home'
import Profile from './pages/Profile'
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Error from './pages/Error'
import Products from './pages/Products'
import AddProducts from './pages/AddProducts'
import './App.css';
import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = ()=>{
    localStorage.clear();
  }; 

  useEffect(()=>{
    fetch('https://capstone2-combong.herokuapp.com/users/getUserDetails',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data=> {
      // console.log(data);
      if (typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        }) 
      } else {
          setUser({
            id:null,
            isAdmin: null
          })
        }
    })
  },[])


  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar/>
        <div>
          <Routes>
            <Route exact path='/' element={<Home/>}/>
            <Route exact path='/products' element={<Products/>}/>
            <Route exact path='/profile' element={<Profile/>}/>
            <Route exact path='/addProducts' element={<AddProducts/>}/>
            <Route exact path='/productsView/:productId' element={<ProductView/>}/>
            <Route exact path='/register' element={<Register/>}/>
            <Route exact path='/login' element={<Login/>}/>
            <Route exact path='/logout' element={<Logout/>}/>
            <Route exact path='*' element={<Error/>}/>
          </Routes>
        </div>
      </Router>
    </UserProvider>

    </>
  )
}

export default App;
