import {useContext} from 'react'
import {Link} from 'react-router-dom'
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext'
export default function AppNavbar(){

  const {user} = useContext(UserContext);

	return(
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={Link} to='/' className="siteName">AutoXperts</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
          </Nav>
          <Nav>
            <Nav.Link as={Link} to='/' className="navFont">Home</Nav.Link>
            <Nav.Link as={Link} to='/products' className="navFont">Products</Nav.Link>
            {
              (user.id !== null) ?
              <>
              <Nav.Link as={Link} to="/profile" className="navFont">Profile</Nav.Link>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              </>
              :
              <>
              <Nav.Link as={Link} to='/login' className="navFont">Login</Nav.Link>
              <Nav.Link as={Link} to='/register'className="navFont">Register</Nav.Link>
            </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)
};