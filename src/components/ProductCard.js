
import {Container, Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function ProductCard(props) {
	// console.log(props)
	const {name, description, price, _id} = props.productProp;

	return(
		<Container>
		<Card 
			bg="dark"
			text="white"
			className="productCourse mb-5 "
		>
		<div className="cardBg">
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
				{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>
				{price}
				</Card.Text>
				<Link className="btn btn-light" to={`/productsView/${_id}`}>View Details</Link>
			</Card.Body>
		</div>
		</Card>
		</Container>
	)
}