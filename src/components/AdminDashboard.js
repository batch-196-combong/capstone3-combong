import React, {useState, useEffect, useRef} from 'react'
import {Container,Card, Button, Modal, Form} from 'react-bootstrap'
import Swal from 'sweetalert2'




export default function AdminDashboard(props){

	// console.log(props)
	const {name, description, price, _id, isActive} = props.productProp;
	const [modal, setModal] = useState(false)
	const [editName, setEditName] = useState(name)
	const [editDescription, setEditDescription] = useState(description)
	const [editPrice, setEditPrice] = useState(price)
	const [editIsActive, setEditIsActive] = useState(isActive)

	const initialRender = useRef(true)
	const secondRender = useRef(true)

	const toggleModal = () => {
		setModal(!modal)
	}

	const toggleActivity = () => {
		setEditIsActive(!editIsActive)
	}

	useEffect(()=>{
		if(initialRender.current){
			initialRender.current = false
		} else { 
			if(secondRender.current){
				secondRender.current = false
			} else {
			if(editIsActive === false){
				fetch(`https://capstone2-combong.herokuapp.com/products/archiveProducts/${_id}`,{
					method: 'DELETE',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				}) 
				.then(res => res.json())
				.then(data => {
					Swal.fire({
						title: 'Archived Successfully',
						icon: 'success',
						text: 'The Proudct is Now Unavilable'
					})
					window.location.reload(false)
				})
			} else {
				fetch(`https://capstone2-combong.herokuapp.com/products/activateProducts/${_id}`,{
					method: 'PUT',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				}) 
				.then(res => res.json())
				.then(data => {
					Swal.fire({
						title: 'Activated Successfully',
						icon: 'success',
						text: 'The Product is Now Available'
					})
					window.location.reload(false)
				})
			}
		}
	}
	}, [editIsActive, _id])

	function editDetails(){

		fetch(`https://capstone2-combong.herokuapp.com/products/updateProducts/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify ({
				name: editName,
				description: editDescription,
				price: editPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				title: 'Edited Successfully',
				icon: 'success',
				text: 'The Proudct is updated'
			})
			window.location.reload(false)
		})
	}

	function submitButton(){
		toggleModal();
		editDetails();
		
	}

	
	return (
	<>
		<Container>
		<Card 
			bg="dark"
			text="white"
			className="productCourse mb-5 "
		>
		<div className="profileCardBg">
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
				{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>
				{price}
				</Card.Text>
				<Button variant="outline-light" className="me-2 btn-modal" onClick={toggleModal}>Edit Details</Button>
				{
					(isActive === true) ?
					<Button variant="outline-light" onClick={toggleActivity}>Disable</Button>
					:
					<Button variant="outline-light" onClick={toggleActivity}>Enable</Button>
				}
			</Card.Body>
		</div>
		</Card>
		</Container>
		{modal &&(
			<div className="mt-5">
			<Form>
			<Modal show={modal} onHide={toggleModal} size="lg"
      			aria-labelledby="contained-modal-title-vcenter"
      			centered>
			        <Modal.Body>
			        	<Form.Group controlId="productName">
			        		<Form.Label>Product Name</Form.Label>
			        		<Form.Control
			        			type="text"
			        			placeholder={name}
			        			value={editName}
			        			onChange={e => setEditName(e.target.value)}
			        		/>
			        	</Form.Group>
			        	<Form.Group controlId="productDescription">
			        		<Form.Label>Product Description</Form.Label>
			        		<Form.Control
			        			type="text"
			        			placeholder={description}
			        			value={editDescription}
			        			onChange={e => setEditDescription(e.target.value)}
			        		/>
			        	</Form.Group>
			        	<Form.Group controlId="productPrice">
			        		<Form.Label>Product Price</Form.Label>
			        		<Form.Control
			        			type="number"
			        			placeholder={price}
			        			value={editPrice}
			        			onChange={e => setEditPrice(e.target.value)}
			        		/>
			        	</Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={toggleModal}>
			            Close
			          </Button>
			          <Button variant="primary" type="submit" id="submitBtn" onClick={submitButton}>
			            Save Changes
			          </Button>
			          </Modal.Footer>
			     </Modal>
			     </Form>
			  </div>
			)}
		</>
	)
	
}


