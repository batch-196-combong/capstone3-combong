import {useState, useEffect} from 'react'
import {useNavigate, Link} from 'react-router-dom'
import {Container, Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function Register(){

	const history = useNavigate();

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [isActive, setIsActive] = useState(false)

	function addProducts(e){
		e.preventDefault()
		fetch('https://capstone2-combong.herokuapp.com/products/addProducts',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res=>res.json())
		.then(data => 
			Swal.fire({
				title: 'Product Added Successful',
				icon: 'success',
				text: 'Product is already on the list'
			}),
			history('/profile')	
		) 

		setName('')
		setDescription('')
		setPrice(0)
	}


	useEffect(()=>{
		if(name !== '' && description !== '' && price !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, description, price]);

	return (
		<>
		<div>
		<Container className="pt-5">
		<h1>Add Products</h1>

		<Form onSubmit={e=>addProducts(e)}>
		<Form.Group controlId="name">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter the product name here"
					required
					value= {name}
					onChange= {e => setName(e.target.value)}
				/>
			</Form.Group>

		<Form.Group controlId="description">
				<Form.Label>Description</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter the product description here"
					required
					value= {description}
					onChange= {e => setDescription(e.target.value)}
				/>
			</Form.Group>

		<Form.Group controlId="price">
				<Form.Label>Product Price</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter product price here"
					required
					value= {price}
					onChange= {e => setPrice(e.target.value)}
				/>
			</Form.Group>

			{ isActive ?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-2">
				Add Products
			</Button>
				:
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-2" disabled>
				Add Products
			</Button>
			}


		</Form>
		<Button variant="primary" type="submit" id="submitBtn" as={Link} to='/profile'>Go Back to Dashboard</Button>
		</Container>
		</div>
		</>
	)
}