import {useState, useEffect} from 'react'
import {Container, Table, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import AdminDashboard from '../components/AdminDashboard';

export default function Profile() {

	const [products, setProducts] = useState([]);
	const [userDetails, setUserDetails] = useState({
		firstName: null,
		lastName: null,
		email: null,
		mobileNo: null,
		isAdmin: null

	})

	useEffect(()=>{
    fetch('https://capstone2-combong.herokuapp.com/users/getUserDetails',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data=> {
      // console.log(data);
      setUserDetails({
      	firstName: data.firstName,
      	lastName: data.lastName,
      	email: data.email,
      	mobileNo: data.mobileNo,
      	isAdmin: data.isAdmin
      })
  	})
	},[])

	useEffect(()=>{
		fetch('https://capstone2-combong.herokuapp.com/products',{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return (
					<AdminDashboard key={product._id} productProp={product}/>
				)
			}))
		})
	},[])

	return(
		<div>
		<Container className="pt-5">
		<Table striped bordered hover variant="dark">
		      <thead>
		        <tr>
		          <th colSpan={2} className="text-center">Profile</th>
		          
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td>First Name</td>
		          <td>{userDetails.firstName}</td>
		          
		        </tr>
		        <tr>
		          <td>Last Name</td>
		          <td>{userDetails.lastName}</td>
		        </tr>
		        
		        <tr>
		          <td>Email</td>
		          <td>{userDetails.email}</td>
		        </tr>
		        
		        <tr>
		          <td>Mobile No.</td>
		          <td>{userDetails.mobileNo}</td>
		        </tr>
		        
		      </tbody>
		    </Table>
		 </Container>
		 {
		 (userDetails.isAdmin === false) ?
		 <>
		 </>
		 :
		 <>
		 	<h1 className="text-center text-dark ">Admin Dashboard</h1>
		 	<div className="text-center mb-4">
		 	<Button variant="outline-dark" as={Link} to='/addProducts'>Add Products</Button>
		 	</div>
		 	{products}
		 </>
		 }
		</div>
	)
}

   
		          
		           