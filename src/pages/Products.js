import {useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard';

export default function Product(){

	const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch('https://capstone2-combong.herokuapp.com/products/activeProducts')
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return (
					<ProductCard key={product.id} productProp={product}/>
				)
			}))
		})
	},[])

	return(
		<>
		<div>
			<h1 className="text-center text-dark">Available Products</h1>
			{products}
		</div>
		</>
	)
}