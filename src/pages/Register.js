import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'
import {Container, Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

	const {user} = useContext(UserContext)
	const history = useNavigate();

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){
		e.preventDefault()
		fetch('https://capstone2-combong.herokuapp.com/users/checkEmailExists',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email you're trying to register is already used"
				})
			} else {
			fetch('https://capstone2-combong.herokuapp.com/users',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				if (data.email){
					Swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						text: 'Thank you for registering'
					})
					history('/login')
				} else {
					Swal.fire({
						title: 'Registration failed',
						icon: 'error',
						text: 'Something went wrong, try again'
					})
				}
			})
		}
		}) 

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');
		
	}


	useEffect(()=>{
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email, firstName, lastName, mobileNo, password]);

	return (
		(user.id !== null) ?
		<Navigate to='/products'/>
		:
		<>
		<div 
			className="loginBg"
			style={{height: 665}}
		>
		<Container className="pt-5">
		<h1>Register Here</h1>

		<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your first name here"
					required
					value= {firstName}
					onChange= {e => setFirstName(e.target.value)}
				/>
			</Form.Group>

		<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your last name here"
					required
					value= {lastName}
					onChange= {e => setLastName(e.target.value)}
				/>
			</Form.Group>

		<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your mobile number here"
					required
					value= {mobileNo}
					onChange= {e => setMobileNo(e.target.value)}
				/>
			</Form.Group>


		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text id="smallText">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your password here"
					required
					value= {password}
					onChange= {e => setPassword(e.target.value)}
				/>
			</Form.Group>

			{ isActive ?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
				Register
			</Button>
				:
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Register
			</Button>
			}
		</Form>
		</Container>
		</div>
		</>
	)
}