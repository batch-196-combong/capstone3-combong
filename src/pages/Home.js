import {Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner (){
	return(
		<>
		<div className='bg text-center'>
			<h1 className="homeText">Start your Journey with AutoXperts</h1>
			<div className="homeBtn">
			<Button as={Link} to='/products' variant="outline-light">See Products</Button>
			</div>
		</div>
		</>
	)
}